// -----------------------------------------------------------------------------
// File: main.c
// -----------------------------------------------------------------------------

//////////////
// Includes //
//////////////

// Standard Includes.
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

// Project Includes.
#include "queue.h"

void print_queue(queue_t *queue) {
    uint64_t num_printed = 0;
    uint64_t idx = queue->first_idx;
    while (num_printed != queue->num_entries) {
        printf(" * %" PRIu64 "\n", queue->entries[idx]);
        num_printed += 1;
        idx = (idx + 1) % queue->max_entries;
    }
}

int main() {
    printf("Hello.\n");

    queue_t *queue = queue_alloc();

    queue_insert_right(queue, 5);
    queue_insert_right(queue, 6);
    queue_insert_right(queue, 4);
    queue_insert_right(queue, 7);
    queue_insert_right(queue, 8);

    printf("Queue\n");
    print_queue(queue);

    uint64_t e1 = queue_remove_left(queue).element;
    uint64_t e2 = queue_remove_left(queue).element;

    printf("Queue\n");
    print_queue(queue);

    printf("Bye.\n");

    return 0;
}
