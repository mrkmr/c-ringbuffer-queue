// -----------------------------------------------------------------------------
// File: queue.h
// -----------------------------------------------------------------------------
//
// Implements a dynamically sized queue which uses an array for backing storage
// for good performance and possible intermediate value O(1) lookup.

//////////////
// Includes //
//////////////

#include <stdint.h>
#include <stdbool.h>

static const uint64_t QUEUE_INITIAL_SIZE = 1;


/////////////
// Structs //
/////////////

// @struct The queue structure.
typedef struct queue {
    uint64_t *entries;
    uint64_t num_entries; // Number of actual entries in the queue.
    uint64_t max_entries; // Max possible number of entries.'

    uint64_t first_idx;
    uint64_t last_idx;

    bool (* insert) (uint64_t);
} queue_t;


// @struct What queue_remove will return.
typedef struct queue_opt_t {
    uint64_t element;
    bool present; // Is the element present? Is returned if the queue is empty.
} queue_opt_t;


/////////////////////////
// Function Prototypes //
/////////////////////////

// @function Allocates a queue.
// @return   The allocated queue.
queue_t *queue_alloc();

// @function Deallocate the queue.
void queue_dealloc(queue_t *queue);


// @function Inserts an element into a queue.
// @return If the operation was successful. If not, there may be no memory left
//         in the queue and on the system (for growing the internal buffer).
bool queue_insert_right(queue_t *queue, uint64_t element);

// @function Remove the left most element from the queue.
// @return The element removed. Or if there were no elements in the queue, then
//         the present field will be false.
queue_opt_t queue_remove_left(queue_t *queue);

// @function Peek at the left most element.
// @return The element at the left. Or if there were no elements in the queue,
//         then the present field will be false.
queue_opt_t queue_peek_left(queue_t *queue);
