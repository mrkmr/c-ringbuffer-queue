// -----------------------------------------------------------------------------
// File: 
// -----------------------------------------------------------------------------
//
// Implementation of queue functions.

//////////////
// Includes //
//////////////

// Standard
#include <stdlib.h>
#include <string.h>

// Project
#include "queue.h"


///////////////
// Functions //
///////////////

queue_t *queue_alloc() {
    // Allocate memory for queue struct.
    queue_t *queue = (queue_t *) malloc(sizeof(queue_t));
    if (queue == NULL) {
        return NULL;
    }

    // Allocate a few entries to start off with.
    queue->entries = (uint64_t *) malloc(sizeof(uint64_t) * QUEUE_INITIAL_SIZE);
    if (queue->entries == NULL) {
        free(queue);
        return NULL;
    }

    queue->first_idx   = 0;
    queue->last_idx    = 0;
    queue->num_entries = 0;
    queue->max_entries = QUEUE_INITIAL_SIZE;

    return queue;
}


void queue_dealloc(queue_t *queue) {
    free(queue->entries);
    free(queue);
}


bool queue_insert_right(queue_t *queue, uint64_t element) {
    // Grow if at capacity.
    if (queue->num_entries == queue->max_entries) {
        uint64_t old_max_entries = queue->max_entries;
        uint64_t new_max_entries = (2 * queue->max_entries);
        uint64_t *new_entries = reallocarray(queue->entries, new_max_entries, sizeof(uint64_t));
        if (new_entries == NULL) {
            return false;
        }

        queue->entries     = new_entries;
        queue->max_entries = new_max_entries;

        // If ring buffer has wrapped around, we need to do some copying.
        if (queue->last_idx <= queue->first_idx) {
            uint64_t wrap_size = queue->last_idx;
            uint64_t last_half = new_max_entries - old_max_entries;

            // Wrap size *will* be less than or equal to half because we just
            // doubled the size.
            memcpy(
                &(queue->entries[old_max_entries]),
                &(queue->entries[0]),
                wrap_size * sizeof(uint64_t)
            );

            queue->last_idx = old_max_entries + wrap_size;
        }
    }

    queue->entries[queue->last_idx] = element;

    queue->num_entries += 1;
    queue->last_idx = ((queue->last_idx + 1) % queue->max_entries);

    return true;
}


queue_opt_t queue_remove_left(queue_t *queue) {
    if (queue->num_entries == 0) {
        queue_opt_t ret = {
            .element = 0,
            .present = false,
        };

        return ret;
    }

    // Store the element to be removed/returned.
    uint64_t element = queue->entries[queue->first_idx];

    // Remove element.
    queue->first_idx = ((queue->first_idx + 1) % queue->max_entries);
    queue->num_entries -= 1;

    queue_opt_t ret = {
        .element = element,
        .present = true,
    };

    return ret;
}


queue_opt_t queue_peek_left(queue_t *queue) {
    queue_opt_t ret;

    if (queue->num_entries == 0) {
        ret.present = false;

        return ret;
    }

    ret.present = true;
    ret.element = queue->entries[queue->first_idx];

    return ret;
}
