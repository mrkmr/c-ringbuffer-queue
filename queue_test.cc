// -----------------------------------------------------------------------------
// File: 
// -----------------------------------------------------------------------------

// Project
extern "C" {
  #include "queue.h"
}

// Third-party.
#include <gtest/gtest.h>

// Demonstrate some basic assertions.
TEST(HelloTest, BasicAssertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);


  queue_t *queue = queue_alloc();


  queue_opt_t ret = queue_peek_left(queue);
  EXPECT_EQ(ret.present, false);


  queue_insert_right(queue, 5);

  ret = queue_peek_left(queue);
  EXPECT_EQ(ret.present, true);
  EXPECT_EQ(ret.element, 5);


  queue_insert_right(queue, 6);
  queue_insert_right(queue, 4);
  queue_insert_right(queue, 7);
  queue_insert_right(queue, 8);

  ret = queue_peek_left(queue);
  EXPECT_EQ(ret.present, true);
  EXPECT_EQ(ret.element, 5);

  queue_remove_left(queue);
  ret = queue_peek_left(queue);
  EXPECT_EQ(ret.present, true);
  EXPECT_EQ(ret.element, 6);

  ret = queue_remove_left(queue);
  EXPECT_EQ(ret.present, true);
  EXPECT_EQ(ret.element, 5);
  ret = queue_peek_left(queue);
  EXPECT_EQ(ret.present, true);
  EXPECT_EQ(ret.element, 6);

  queue_dealloc(queue);
}
